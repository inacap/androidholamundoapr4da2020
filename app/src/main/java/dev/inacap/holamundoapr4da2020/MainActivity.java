package dev.inacap.holamundoapr4da2020;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import dev.inacap.holamundoapr4da2020.vistas.HomeActivity;

public class MainActivity extends AppCompatActivity {
    // Declarar variables de los Widgets
    EditText etNombreUsuario;
    EditText etPass;
    Button btIngresar;
    TextView tvOvidePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instanciar
        etNombreUsuario = findViewById(R.id.etNombreUsuario);
        etPass = findViewById(R.id.etPassword);
        btIngresar = findViewById(R.id.btIngresar);
        tvOvidePass = findViewById(R.id.tvRecuperarPass);

        // Interactuar

        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Iniciar una segunda Activity
                Intent segundaActivity = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(segundaActivity);
            }
        });
    }
}
