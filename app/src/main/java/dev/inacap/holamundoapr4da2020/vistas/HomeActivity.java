package dev.inacap.holamundoapr4da2020.vistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import dev.inacap.holamundoapr4da2020.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
